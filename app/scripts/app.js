'use strict';

define(['underscore', 'backbone', 'scripts/routers/router', 'scripts/routers/movieRouter', 'scripts/routers/tvshowRouter'], function(_, Backbone, Router, MovieRouter, TvshowRouter) {
	var app = {
		init: function() {

			window.app = this;

			this.router = new Router();
			this.movieRouter = new MovieRouter();
			this.tvshowRouter = new TvshowRouter();

			Backbone.history.start({
				pushState: false
					//hashChange: false
			});

			this.router.navigate('/', {
				trigger: true
			});
		}
	};

	return app;
});
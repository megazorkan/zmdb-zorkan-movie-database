'use strict';

define(['backbone'], function(Backbone) {

	var onlyCollection = Backbone.Collection.extend({

		//sad u ovu jednu jedinu kolekciju mogu trpat sve ove, jer svi rade isto u ovom slucaju
		//samo sluzi za drzanje elemenata
		initialize: function(options) {
			this.options = options || {};
		},

		getModels: function(data) {
			var that = this;
			_.each(data, function(element) {
				var newElement = new that.options.customModel();
				newElement.setData(element);
				that.add(newElement);
			});
			that.shift();
		},


	});

	return onlyCollection;

});
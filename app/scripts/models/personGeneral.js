'use strict';

define(['backbone', 'scripts/models/base', '../services/globals'], function(Backbone, BaseModel, Globals) {
	var personGeneral = BaseModel.extend({

		defaults: {
			'biography': '',
			'birthday': '',
			'deathday': '',
			'homepage': '',
			'id': '',
			'name': '',
			'placeOfBirth': '',
			'profilePath': ''

		},

		initialize: function() {
			this.on('change:profile_path', this.setProfilePath, this);
		},

		setData: function(person) {
			this.set('biography', person.biography);
			this.set('birthday', person.birthday);
			this.set('deathday', person.deathday);
			this.set('homepage', person.homepage);
			this.set('id', person.id);
			this.set('name', person.name);
			this.set('placeOfBirth', person.place_of_birth);
			this.set('profilePath', Globals.imageBaseUrl + person.profile_path);
		},

		setUrl: function(url) {
			this.url = url;
		},

		setProfilePath: function() {
			if (this.get('profile_path') === null) {
				this.set('profilePath', Globals.replacementImage);
			} else {
				console.log('namjestam');
				this.set('profilePath', Globals.imageBaseUrl + this.get('profile_path'));
			}
		}

	});
	return personGeneral;
});
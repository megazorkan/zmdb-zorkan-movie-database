'use strict';

define(['backbone', 'scripts/models/base', '../services/globals'], function(Backbone, BaseModel, Globals) {
	var crewMember = BaseModel.extend({

		defaults: {
			id: 0,
			job: '',
			name: '',
			profilePath: ''

		},

		setData: function(castMember) {
			this.set('id', castMember.id);
			this.set('job', castMember.job);
			this.set('name', castMember.name);
			if (crewMember.profile_path !== null) {
				this.set('profilePath', Globals.imageBaseUrl + castMember.profile_path);
			} else {
				this.set('profilePath', Globals.replacementImage);
			}

		}

	});
	return crewMember;
});
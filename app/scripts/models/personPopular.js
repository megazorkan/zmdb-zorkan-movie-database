'use strict';

define(['backbone', 'scripts/models/base', '../services/globals', '../collections/onlyCollection', '../models/movieBasicInformation'],
	function(Backbone, BaseModel, Globals, OnlyCollection, MovieBasicInformationModel) {
		var personPopular = BaseModel.extend({

			defaults: {
				'id': '',
				'name': '',
				'profilePath': '',
				'knownForMovies': '',
				'profileLink': ''

			},

			initialize: function() {
				this.on('change:profile_path', this.setProfilePath, this);

			},

			setData: function(person) {
				this.set('id', person.id);
				this.set('name', person.name);
				this.set('profilePath', Globals.imageBaseUrl + person.profile_path);
				this.set('profileLink', this.getPersonLink(person.id));
				var knownForMoviesCollection = new OnlyCollection({
					customModel: MovieBasicInformationModel
				});
				knownForMoviesCollection.getModels(person.known_for);

				this.set('knownForMovies', knownForMoviesCollection);
			},

			setProfilePath: function() {
				if (this.get('profile_path') === null) {
					this.set('profilePath', Globals.replacementImage);
				} else {
					console.log('namjestam');
					this.set('profilePath', Globals.imageBaseUrl + this.get('profile_path'));
				}
			},

			getPersonLink: function(id) {
				return '#/person/' + id;
			}

		});
		return personPopular;
	});
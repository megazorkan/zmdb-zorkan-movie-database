'use strict';

define(['backbone', 'scripts/models/base', '../services/globals'], function(Backbone, BaseModel, Globals) {
	var poster = BaseModel.extend({

		defaults: {
			filePath: '',
			height: 0,
			width: 0,
			voteAverage: 0,
			voteCount: 0

		},

		setData: function(poster) {;
			this.set('filePath', Globals.imageBaseUrl + poster.file_path);
			this.set('height', poster.height);
			this.set('width', poster.width);
			this.set('voteAverage', poster.vote_average);
			this.set('voteCount', poster.vote_count);
		}

	});
	return poster;
});
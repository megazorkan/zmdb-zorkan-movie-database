'use strict';

/*
Since TMDB API returns a response in such way that it sends only one model with 
results array(collection) as models attribute, each collection has this fetcher inside it, 
and then collection uses function to populate itself with results from this model
*/

define(['backbone'], function(Backbone) {
	var fetcher = Backbone.Model.extend({
		initialize: function(url) {
			this.url = url;
		},

		setUrl: function(url) {
			this.url = url;
		}
	});
	return fetcher;
});
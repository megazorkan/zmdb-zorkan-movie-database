'use strict';

define(['backbone', 'scripts/models/base', '../services/globals'], function(Backbone, BaseModel, Globals) {
	var movieBasicInformation = BaseModel.extend({
		//ne zaboravi dodati i ostale basic informacije ko 
		//prodaksn kompanise kountrije hompeage budget itd
		defaults: {
			id: 0,
			adult: false,
			originalTitle: '',
			originalLanguage: '',
			overview: '',
			releaseDate: '',
			genreIds: {},
			voteAverage: 0,
			voteCount: 0,
			posterPath: ''

		},


		setData: function(movie) {
			this.set('id', movie.id);
			this.set('adult', movie.adult);
			this.set('originalTitle', movie.original_title);
			this.set('originalLanguage', movie.original_language);
			this.set('overview', movie.overview);
			this.set('releaseDate', movie.release_date);
			this.set('genreIds', movie.genre_ids);
			this.set('voteAverage', movie.vote_average);
			this.set('voteCount', movie.vote_count);
			if (movie.profile_path !== null) {
				this.set('posterPath', Globals.imageBaseUrl + movie.poster_path);
			} else {
				this.set('posterPath', Globals.replacementImage);
			}

		},

		setUrl: function(url) {
			this.url = url;
		}

	});
	return movieBasicInformation;
});
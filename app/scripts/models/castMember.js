'use strict';

define(['backbone', 'scripts/models/base', '../services/globals'], function(Backbone, BaseModel, Globals) {
	var castMember = BaseModel.extend({

		defaults: {
			id: 0,
			character: '',
			name: '',
			profilePath: ''

		},

		setData: function(castMember) {
			this.set('id', castMember.id);
			this.set('character', castMember.character);
			this.set('name', castMember.name);
			if (castMember.profile_path !== null) {
				this.set('profilePath', Globals.imageBaseUrl + castMember.profile_path);
			} else {
				this.set('profilePath', Globals.replacementImage);
			}
		}

	});
	return castMember;
});
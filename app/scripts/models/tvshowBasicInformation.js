'use strict';

define(['backbone', 'scripts/models/base', '../services/globals'], function(Backbone, BaseModel, Globals) {
	var tvshowBasicInformation = BaseModel.extend({

		defaults: {
			id: 0,
			originCountry: {},
			originalName: '',
			overview: '',
			posterPath: '',
			popularity: 0,
			firstAirDate: '',
			voteAverage: 0,
			voteCount: 0
		},

		setData: function(tvshow) {
			this.set('id', tvshow.id);
			this.set('originCountry', tvshow.origin_country);
			this.set('originalName', tvshow.original_name);
			this.set('name', tvshow.name);
			this.set('overview', tvshow.overview);
			this.set('popularity', tvshow.popularity);
			this.set('firstAirDate', tvshow.first_air_date);
			this.set('voteAverage', tvshow.vote_average);
			this.set('voteCount', tvshow.vote_count);
			if (tvshow.profile_path !== null) {
				this.set('posterPath', Globals.imageBaseUrl + tvshow.poster_path);
			} else {
				this.set('posterPath', Globals.replacementImage);
			}
		},

		setUrl: function(url) {
			this.url = url;
		}

	});
	return tvshowBasicInformation;
});
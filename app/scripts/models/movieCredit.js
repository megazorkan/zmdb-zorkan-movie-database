'use strict';

define(['backbone', 'scripts/models/base', '../services/globals'], function(Backbone, BaseModel, Globals) {
	var movieCredit = BaseModel.extend({

		defaults: {
			character: '',
			originalTitle: '',
			posterPath: '',
			releaseDate: '',
			title: ''

		},

		setData: function(movieCredit) {
			this.set('character', movieCredit.character);
			this.set('originalTitle', movieCredit.original_title);
			if (movieCredit.poster_path !== null) {
				this.set('posterPath', Globals.imageBaseUrl + movieCredit.profile_path);
			} else {
				this.set('profilePath', Globals.replacementImage);
			}
			this.set('releaseDate', movieCredit.release_date);
			this.set('title', movieCredit.title);
		}

	});
	return movieCredit;
});
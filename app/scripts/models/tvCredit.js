'use strict';

define(['backbone', 'scripts/models/base', '../services/globals'], function(Backbone, BaseModel, Globals) {
	var tvCredit = BaseModel.extend({

		defaults: {
			character: '',
			episodeCount: 0,
			originalName: '',
			posterPath: ''

		},

		setData: function(tvCredit) {;
			this.set('character', tvCredit.character);
			this.set('episodeCount', tvCredit.episode_count);
			this.set('originalName', tvCredit.original_name);
			if (tvCredit.poster_path !== null) {
				this.set('posterPath', Globals.imageBaseUrl + tvCredit.profile_path);
			} else {
				this.set('profilePath', Globals.replacementImage);
			}
		}

	});
	return tvCredit;
});
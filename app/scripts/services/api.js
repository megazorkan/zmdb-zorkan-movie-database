'use strict';

define(['../models/movieBasicInformation', '../models/tvshowBasicInformation', '../models/fetcher',
		'../collections/onlyCollection'
	],
	function(MovieBasicInformationModel, TvshowBasicInformationModel, FetcherModel,
		OnlyCollection) {

		var api = (function() {
			var instantiated;

			var apiKey = '25db7eb1def81f6bb554266ca8460b8f';

			var baseUrl = 'http://api.themoviedb.org/3/';

			var fetcher = new FetcherModel();

			var url = '';


			//singleton functions go here
			function init() {
				return {


					discover: function(type) {
						url = baseUrl + 'discover/' + type + '?api_key=' + apiKey;
						fetcher.setUrl(url);
						return fetcher.fetch();
					},

					search: function(type, term) {
						url = baseUrl + 'search/' + type + '?api_key=' + apiKey + '&query=' + term;
						fetcher.setUrl(url);
						return fetcher.fetch();
					},

					/**********************
						PEOPLE
					**********************/

					getPeopleByChart: function(chart) {
						url = baseUrl + 'person/' + chart + '?api_key=' + apiKey;
						fetcher.setUrl(url);
						return fetcher.fetch();
					},

					getPersonInfo: function(id, info) {
						info === undefined ? info = '' : info = '/' + info;
						url = baseUrl + 'person/' + id + info + '?api_key=' + apiKey;
						fetcher.setUrl(url);
						return fetcher.fetch();
					},
					/*********************
						MOVIES
					**********************/

					getMoviesByChart: function(chart) {
						url = baseUrl + 'movie/' + chart + '?api_key=' + apiKey;
						fetcher.setUrl(url);
						return fetcher.fetch();
					},

					getMovieInfo: function(id, info) {
						info === undefined ? info = '' : info = '/' + info;
						url = baseUrl + 'movie/' + id + info + '?api_key=' + apiKey;
						fetcher.setUrl(url);
						return fetcher.fetch();
					},

					/********************
						TV SHOWS
					********************/

					getTvshowsByChart: function(chart) {
						url = baseUrl + 'tv/' + chart + '?api_key=' + apiKey;
						fetcher.setUrl(url);
						return fetcher.fetch();
					},

					getTvshowInfo: function(id, info) {
						info === undefined ? info = '' : info = '/' + info;
						url = baseUrl + 'tv/' + id + info + '?api_key=' + apiKey;
						fetcher.setUrl(url);
						return fetcher.fetch();
					},

				};
			}

			//makes api class a singleton, allowing it to be instantiated only once
			return {
				getInstance: function() {
					if (!instantiated) {
						instantiated = init();
					}
					return instantiated;
				}
			};

		})();

		return api;
	});
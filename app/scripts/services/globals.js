'use strict';

define([], function() {

	var globals = {
		'imageBaseUrl': 'http://image.tmdb.org/t/p/w500',

		'replacementImage': 'images/replacement.png',

	};
	return globals;
});
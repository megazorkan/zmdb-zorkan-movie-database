/*global require*/
'use strict';

require.config({
	baseUrl: '',

	shim: {
		'underscore': {
			exports: '_'
		},
		'backbone': {
			deps: ['underscore', 'jquery'],
			exports: 'Backbone'
		},
		'handlebars': {
			exports: 'Handlebars'
		}
	},

	paths: {
		jquery: '../bower_components/jquery/dist/jquery',
		backbone: '../bower_components/backbone/backbone',
		underscore: '../bower_components/underscore/underscore',
		handlebars: '../bower_components/handlebars/handlebars',
		templates: '../.tmp/templates'
	}
});

require(['backbone', 'handlebars', 'scripts/app'], function(Backbone, Handlebars, app) {

	Handlebars.registerHelper('getYearFromDate', function(date) {
		return date.substring(0, 4);
	});

	Backbone.View.prototype.close = function() {
		if (this.onClose) {
			this.onClose();
		}
		this.remove();
	};

	app.init();
});
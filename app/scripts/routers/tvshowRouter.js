'use strict';

define(['backbone',
		'scripts/views/tvshow', 'scripts/views/tvshows', 'scripts/views/error',
		'scripts/collections/onlyCollection',
		'scripts/models/tvshowBasicInformation', 'scripts/models/castMember', 'scripts/models/crewMember', 'scripts/models/poster',
		'scripts/services/api'
	],
	function(Backbone,
		TvshowView, TvshowsView, ErrorView,
		OnlyCollection,
		TvshowBasicInformationModel, CastMemberModel, CrewMemberModel, PosterModel,
		api) {

		var tvshowRouter = Backbone.Router.extend({
			initialize: function() {},

			routes: {
				'tvshows': 'toprated',
				'tvshow/:id': 'tvshow',
				'tvshows/toprated': 'toprated',
				'tvshows/popular': 'popular',
				'tvshows/ontheair': 'ontheair',
				'tvshows/airingtoday': 'airingtoday'
			},


			tvshow: function(id) {
				var that = this;

				var basicInformationPromise = api.getInstance().getTvshowInfo(id),
					castPromise = api.getInstance().getTvshowInfo(id, 'credits'),
					postersPromise = api.getInstance().getTvshowInfo(id, 'images'),
					similarPromise = api.getInstance().getTvshowInfo(id, 'similar');

				$.when(basicInformationPromise, castPromise, postersPromise, similarPromise).then(
					function() {

						//BASIC INFORMATION
						var basicInformation = new TvshowBasicInformationModel();
						basicInformation.setData(basicInformationPromise.responseJSON);

						//CAST MEMBERS
						var cast = new OnlyCollection({
							customModel: CastMemberModel
						});
						cast.getModels(castPromise.responseJSON.cast);

						//CREW MEMBERS
						var crew = new OnlyCollection({
							customModel: CrewMemberModel
						});
						crew.getModels(castPromise.responseJSON.crew);

						//POSTERS
						var posters = new OnlyCollection({
							customModel: PosterModel
						});
						posters.getModels(postersPromise.responseJSON.posters);

						//SIMILAR
						var similar = new OnlyCollection({
							customModel: TvshowBasicInformationModel
						});
						similar.getModels(similarPromise.responseJSON.results);

						console.log(basicInformation);
						var tvshowView = new TvshowView({
							id: id,
							basicInformation: basicInformation,
							cast: cast,
							crew: crew,
							posters: posters,
							similar: similar
						});
						that.showView(tvshowView);

					},
					function() {
						that.handleError(that);

					});

			},

			toprated: function() {
				this.tvshowsByChart('top_rated');
			},

			popular: function() {
				this.tvshowsByChart('popular');
			},

			ontheair: function() {
				this.tvshowsByChart('on_the_air');
			},

			airingtoday: function() {
				this.tvshowsByChart('airing_today');
			},

			tvshowsByChart: function(chart) {
				var that = this;

				api.getInstance().getTvshowsByChart(chart).then(
					function(data) {

						//MOVIES
						var tvshows = new OnlyCollection({
							customModel: TvshowBasicInformationModel
						});
						tvshows.getModels(data.results);

						var tvshowsView = new TvshowsView({
							tvshows: tvshows

						});
						that.showView(tvshowsView);
					},

					function() {
						that.handleError(that);
					});
			},

			handleError: function(that) {
				var errorView = new ErrorView();
				that.showView(errorView);
			},

			//function to properly change view 
			showView: function(view) {
				if (this.currentView) {
					this.currentView.close();
				}

				$('#app').html('');
				$('#app').html(view.render().el);
				this.currentView = view;
				return view;
			}
		});

		return tvshowRouter;
	});
'use strict';

define(['backbone',
		'scripts/views/movie', 'scripts/views/movies', 'scripts/views/error',
		'scripts/collections/onlyCollection',
		'scripts/models/movieBasicInformation', 'scripts/models/castMember', 'scripts/models/crewMember', 'scripts/models/poster',
		'scripts/services/api'
	],
	function(Backbone,
		MovieView, MoviesView, ErrorView,
		OnlyCollection,
		MovieBasicInformationModel, CastMemberModel, CrewMemberModel, PosterModel,
		api) {
		var movieRouter = Backbone.Router.extend({
			initialize: function() {},

			routes: {
				'movie/:id': 'movie',
				'movies': 'toprated',
				'topratedmovies': 'toprated',
				'nowplayingmovies': 'nowplaying',
				'popularmovies': 'popular',
				'upcomingmovies': 'upcoming'
			},

			movie: function(id) {

				var that = this;

				var basicInformationPromise = api.getInstance().getMovieInfo(id),
					castMembersPromise = api.getInstance().getMovieInfo(id, 'credits'),
					postersPromise = api.getInstance().getMovieInfo(id, 'images'),
					similarMoviesPromise = api.getInstance().getMovieInfo(id, 'similar');

				$.when(basicInformationPromise, castMembersPromise, postersPromise, similarMoviesPromise).then(
					function() {

						//BASIC INFORMATION
						var basicInformation = new MovieBasicInformationModel();
						basicInformation.setData(basicInformationPromise.responseJSON);

						//CAST MEMBERS
						var castMembers = new OnlyCollection({
							customModel: CastMemberModel
						});
						castMembers.getModels(castMembersPromise.responseJSON.cast);

						//CREW MEMBERS
						var crewMembers = new OnlyCollection({
							customModel: CrewMemberModel
						});
						crewMembers.getModels(castMembersPromise.responseJSON.crew);

						//POSTERS
						var posters = new OnlyCollection({
							customModel: PosterModel
						});
						posters.getModels(postersPromise.responseJSON.posters);

						//SIMILAR
						var similarMovies = new OnlyCollection({
							customModel: MovieBasicInformationModel
						});
						similarMovies.getModels(similarMoviesPromise.responseJSON.results);

						var movieView = new MovieView({
							movie: basicInformation,
							cast: castMembers,
							crew: crewMembers,
							posters: posters,
							similarMovies: similarMovies
						});
						that.showView(movieView);

					},
					function() {
						that.handleError(that);

					});
			},

			toprated: function() {
				this.moviesByChart('top_rated');
			},

			nowplaying: function() {
				this.moviesByChart('now_playing');
			},

			upcoming: function() {
				this.moviesByChart('upcoming');
			},

			popular: function() {
				this.moviesByChart('popular');
			},

			moviesByChart: function(chart) {

				var that = this;

				api.getInstance().getMoviesByChart(chart).then(
					function(data) {

						//MOVIES
						var movies = new OnlyCollection({
							customModel: MovieBasicInformationModel
						});
						movies.getModels(data.results);

						var moviesView = new MoviesView({
							movies: movies

						});
						that.showView(moviesView);
					},

					function() {
						that.handleError(that);
					});
			},

			handleError: function(that) {
				var errorView = new ErrorView();
				that.showView(errorView);
			},

			//function to properly change view 
			showView: function(view) {
				if (this.currentView) {
					this.currentView.close();
				}

				$('#app').html('');
				$('#app').html(view.render().el);
				this.currentView = view;
				return view;
			}
		});

		return movieRouter;
	});
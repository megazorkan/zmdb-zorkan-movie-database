'use strict';

define(['backbone',
		'scripts/views/welcome', 'scripts/views/account', 'scripts/views/person', 'scripts/views/people', 'scripts/views/search', 'scripts/views/error',
		'scripts/collections/onlyCollection',
		'scripts/models/movieBasicInformation', 'scripts/models/tvshowBasicInformation', 'scripts/models/poster', 'scripts/models/personGeneral', 'scripts/models/tvCredit', 'scripts/models/movieCredit', 'scripts/models/personPopular',
		'scripts/services/api'
	],
	function(Backbone,
		WelcomeView, AccountView, PersonView, PeopleView, SearchView, ErrorView,
		OnlyCollection,
		MovieBasicInformationModel, TvshowBasicInformationModel, PosterModel, PersonGeneralModel, TvCreditModel, MovieCreditModel, PersonPopularModel,
		api) {

		var router = Backbone.Router.extend({
			initialize: function() {},

			routes: {
				'': 'welcome',
				'account': 'account',
				'people': 'people',
				'person/:id': 'person',
				'register': 'register',
				'search/:term': 'search',
				'login': 'login'
			},

			welcome: function() {

				var that = this;

				var moviesCollectionPromise = api.getInstance().discover('movie'),
					tvshowsCollectionPromise = api.getInstance().discover('tv');


				$.when(moviesCollectionPromise, tvshowsCollectionPromise).then(
					function() {

						//MOVIES
						var moviesCollection = new OnlyCollection({
							customModel: MovieBasicInformationModel
						});
						moviesCollection.getModels(moviesCollectionPromise.responseJSON.results);

						//TV SHOWS
						var tvshowsCollection = new OnlyCollection({
							customModel: TvshowBasicInformationModel
						});
						tvshowsCollection.getModels(tvshowsCollectionPromise.responseJSON.results);

						//VIEW
						var welcomeView = new WelcomeView({
							onTvCollection: tvshowsCollection,
							inTheatersCollection: moviesCollection
						});

						that.showView(welcomeView);

					},
					function() {
						that.handleError(that);
					});
			},

			person: function(id) {

				var that = this;

				var generalPromise = api.getInstance().getPersonInfo(id),
					postersPromise = api.getInstance().getPersonInfo(id, 'images'),
					movieCreditsPromise = api.getInstance().getPersonInfo(id, 'movie_credits'),
					tvCreditsPromise = api.getInstance().getPersonInfo(id, 'tv_credits');

				$.when(generalPromise, postersPromise, movieCreditsPromise, tvCreditsPromise).then(
					function() {

						//PERSON GENERAL
						var personGeneral = new PersonGeneralModel();
						personGeneral.setData(generalPromise.responseJSON);

						//POSTERS
						var posters = new OnlyCollection({
							customModel: PosterModel
						});
						posters.getModels(postersPromise.responseJSON.profiles);

						//MOVIE CREDITS
						var movieCredits = new OnlyCollection({
							customModel: MovieCreditModel,
						});
						movieCredits.getModels(movieCreditsPromise.responseJSON.cast);

						//TV CREDITS
						var tvCredits = new OnlyCollection({
							customModel: TvCreditModel,
						});
						tvCredits.getModels(tvCreditsPromise.responseJSON.cast);

						var personView = new PersonView({
							id: id,
							personGeneral: personGeneral,
							posters: posters,
							tvCredits: tvCredits,
							movieCredits: movieCredits
						});
						that.showView(personView);

					},
					function() {
						that.handleError(that);
					});
			},

			people: function() {

				var that = this;

				api.getInstance().getPeopleByChart('popular').then(
					function(data) {

						//POPULAR PEOPLE
						var popularPeople = new OnlyCollection({
							customModel: PersonPopularModel,
						});
						popularPeople.getModels(data.results);

						//VIEW
						var peopleView = new PeopleView({
							people: popularPeople
						});
						that.showView(peopleView);
					},

					function() {
						that.handleError(that);
					});
			},

			search: function(term) {

				var that = this;

				api.getInstance().search('multi', term).then(
					function(data) {
						_.each(data.results, function(element) {
							console.log(element);
						});;
						var searchView = new SearchView();
						that.showView(searchView);
					},
					function() {
						that.handleError(that);
					}
				);


			},

			account: function() {
				var that = this;
				var accountView = new AccountView();
				that.showView(accountView);
			},

			register: function() {
				console.log('coming this summer!!!!');
			},

			login: function() {
				console.log('coming this summer!!!!');
			},

			handleError: function(that) {
				var errorView = new ErrorView();
				that.showView(errorView);
			},

			//function to properly change view 
			showView: function(view) {
				if (this.currentView) {
					this.currentView.close();
				}

				$('#app').html('');
				$('#app').html(view.render().el);
				this.currentView = view;
				return view;
			}
		});

		return router;
	});
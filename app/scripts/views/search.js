'use strict';
define(['backbone', 'templates', '../services/api'], function(Backbone, JST, api) {

	var searchView = Backbone.View.extend({

		template: JST['app/scripts/templates/search.hbs'],

		initialize: function(options) {
			this.options = options || {};
		},

		render: function() {
			this.$el.html(this.template());
			return this;
		}
	});

	return searchView;
});
'use strict';
define(['backbone', 'templates'], function(Backbone, JST) {

	var movieView = Backbone.View.extend({

		template: JST['app/scripts/templates/movie.hbs'],

		events: {},

		initialize: function(options) {
			this.options = options || {};
		},

		render: function() {
			this.$el.html(this.template({
				movie: this.options.movie.toJSON(),
				cast: this.options.cast.toJSON(),
				posters: this.options.posters.toJSON(),
				similarMovies: this.options.similarMovies.toJSON()
			}));
			return this;
		},

	});

	return movieView;
});
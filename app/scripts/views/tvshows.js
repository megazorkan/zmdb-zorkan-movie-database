'use strict';
define(['backbone', 'templates'], function(Backbone, JST) {

	var tvShowsView = Backbone.View.extend({

		template: JST['app/scripts/templates/tvshows.hbs'],

		events: {
			'click .menu-item-tvshows': 'navigate',
		},

		initialize: function(options) {
			this.options = options || {};
		},

		render: function() {
			this.$el.html(this.template({
				tvshows: this.options.tvshows.toJSON()
			}));
			return this;
		},

		navigate: function(ev) {
			var address = $(ev.currentTarget).data('address');
			Backbone.history.navigate('/tvshows/' + address, {
				trigger: true
			});
		},

	});

	return tvShowsView;
});
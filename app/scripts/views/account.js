'use strict';
define(['backbone', 'templates'], function(Backbone, JST) {

	var accountView = Backbone.View.extend({

		template: JST['app/scripts/templates/account.hbs'],

		events: {},

		initialize: function() {},

		render: function() {
			this.$el.html(this.template());
			return this;
		},

	});

	return accountView;
});
'use strict';
define(['backbone', 'templates'], function(Backbone, JST) {

	var loginView = Backbone.View.extend({

		template: JST['app/scripts/templates/login.hbs'],

		events: {},

		initialize: function() {},

		render: function() {
			this.$el.html(this.template());
			return this;
		},

	});

	return loginView;
});
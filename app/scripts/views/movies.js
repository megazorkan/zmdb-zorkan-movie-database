'use strict';
define(['backbone', 'templates', '../services/api'], function(Backbone, JST, api) {

	var moviesView = Backbone.View.extend({

		template: JST['app/scripts/templates/movies.hbs'],

		events: {
			'click .menu-item-tvshows': 'navigate'
		},

		initialize: function(options) {
			this.options = options || {};
		},

		render: function() {
			this.$el.html(this.template({
				movies: this.options.movies.toJSON()
			}));
			return this;
		},

		navigate: function(ev) {
			var address = $(ev.currentTarget).data('address');
			Backbone.history.navigate('/tvshows/' + address, {
				trigger: true
			});
		},

	});

	return moviesView;
});
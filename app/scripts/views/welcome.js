'use strict';
define(['backbone', 'templates', '../services/api'], function(Backbone, JST, api) {

	var indexView = Backbone.View.extend({

		template: JST['app/scripts/templates/welcome.hbs'],

		//jebeni search mi ovdje ne radi
		events: {
			'click #multi-search-button': 'multiSearch'
		},

		initialize: function(options) {
			this.options = options || {};
		},

		render: function() {
			this.$el.html(this.template({
				onTvCollection: this.options.onTvCollection.toJSON(),
				inTheatersCollection: this.options.inTheatersCollection.toJSON()
			}));
			return this;

		},

		multiSearch: function(ev) {
			var address = $(ev.currentTarget).data('address');
			var searchTerm = $('#multi-search-input').val();

			console.log('search term: ' + searchTerm);

			Backbone.history.navigate('/search/' + searchTerm, {
				trigger: true
			});
		}

	});

	return indexView;
});
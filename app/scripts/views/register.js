'use strict';
define(['backbone', 'templates'], function(Backbone, JST) {

	var registerView = Backbone.View.extend({

		template: JST['app/scripts/templates/register.hbs'],

		events: {},

		initialize: function() {},

		render: function() {
			this.$el.html(this.template());
			return this;
		},

	});

	return registerView;
});
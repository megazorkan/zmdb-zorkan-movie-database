'use strict';
define(['backbone', 'templates'], function(Backbone, JST) {

	var personView = Backbone.View.extend({

		template: JST['app/scripts/templates/person.hbs'],

		events: {},

		initialize: function(id, options) {
			//ovo mi je precudno ovo sa id-em, zasto tek preko njega se moze prisupit
			this.options = options || {};
			this.options.id = id;
		},

		render: function() {
			this.$el.html(this.template({
				personGeneral: this.options.id.personGeneral.toJSON(),
				posters: this.options.id.posters.toJSON(),
				tvCredits: this.options.id.tvCredits.toJSON(),
				movieCredits: this.options.id.movieCredits.toJSON()
			}));
			return this;
		},

	});

	return personView;
});
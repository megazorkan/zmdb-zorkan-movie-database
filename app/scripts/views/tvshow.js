'use strict';
define(['backbone', 'templates'], function(Backbone, JST) {

	var tvshowView = Backbone.View.extend({

		template: JST['app/scripts/templates/tvshow.hbs'],

		events: {},

		initialize: function(options) {
			this.options = options || {};
		},

		render: function() {
			this.$el.html(this.template({
				basicInformation: this.options.basicInformation.toJSON(),
				cast: this.options.cast.toJSON(),
				posters: this.options.posters.toJSON(),
				similar: this.options.similar.toJSON()
			}));
			return this;
		},

	});

	return tvshowView;
});
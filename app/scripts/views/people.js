'use strict';
define(['backbone', 'templates'], function(Backbone, JST) {

	var peopleView = Backbone.View.extend({

		template: JST['app/scripts/templates/people.hbs'],

		events: {},

		initialize: function(options) {
			this.options = options || {};
			console.log(this.options.people);

		},

		render: function() {
			this.$el.html(this.template({
				people: this.options.people.toJSON()
			}));
			return this;
		},

	});

	return peopleView;
});
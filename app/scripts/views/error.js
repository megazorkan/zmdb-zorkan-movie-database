'use strict';
define(['backbone', 'templates'], function(Backbone, JST) {

	var errorView = Backbone.View.extend({

		template: JST['app/scripts/templates/error.hbs'],

		events: {},

		initialize: function() {},

		render: function() {
			this.$el.html(this.template());
			return this;
		},

	});

	return errorView;
});